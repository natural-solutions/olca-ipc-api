#!/usr/bin/env bash

set -e

if (( $# < 1 )); then
    exit 0
fi

# Pre-pre-flight? 🤷
if [[ -n "$MSYSTEM" ]]; then
    echo "Seems like you are using an MSYS2-based system (such as Git Bash) which is not supported. Please use WSL instead.";
    exit 1
fi

script_file="docker/cmd.sh"
if ! test -f "$script_file"; then
    echo "Please execute this script from the parent of the docker directory"
    echo "The command should be ./docker/cmd.sh"
    exit 1
fi

env_file="docker/.env"
if ! test -f "$env_file"; then
    echo "$env_file file doesn't not exists. Create it from docker/.env.tpl (customize if necessary)"
    exit 1
fi

launch_compose="docker-compose"
if ! command -v ${launch_compose} &> /dev/null
then
    launch_compose="docker compose"
    set +e
    docker compose version &> /dev/null
    if [ $? -ne 0 ]; then
        echo "The 'docker compose' is not available. Please install or update docker."
        exit 1
    fi
    set -e
fi

# shellcheck source=/dev/null
source ./docker/.env

if [ "$ENV" == "production" ]; then
    docker-compose --project-name="${PROJECT}" -f ./docker/docker-compose.yml -f ./docker/docker-compose.prod.yml "$@"
else
    docker-compose --project-name="${PROJECT}" --project-directory=./docker "$@"
fi
