from flask import Flask
app = Flask(__name__)
import olca


client = olca.Client(8082)


@app.route("/")
def hello():

    setup = olca.CalculationSetup()

    # define the calculation type here
    # see http://greendelta.github.io/olca-schema/html/CalculationType.html
    setup.calculation_type = olca.CalculationType.SIMPLE_CALCULATION

    # select the product system and LCIA method
    setup.impact_method = client.find(olca.ImpactMethod, 'ReCiPe 2016 Midpoint (H)')
    setup.product_system = client.find(olca.ProductSystem, 'Broiler,broiler feed,Label Rouge prod, at farm gate/kg')
    
    setup.parameter_redefs = []
    redef1 = olca.ParameterRedef()
    redef1.context = olca.ref(olca.Process,'024abad5-2de9-3fca-a716-a7be15d73b3f')
    redef1.name = 'distance'
    redef1.value = 1000
    setup.parameter_redefs.append(redef1)

    # amount is the amount of the functional unit (fu) of the system that
    # should be used in the calculation; unit, flow property, etc. of the fu
    # can be also defined; by default openLCA will take the settings of the
    # reference flow of the product system
    setup.amount = 1000

    # calculate the result and export it to an Excel file
    result = client.calculate(setup)
    #client.excel_export(result, 'results/result.xlsx')
    json = result.to_json()

    # calculate the upstream tree and traverse it
    """ def traversal_handler(n: tuple[UpstreamNode, int]):
        (node, depth) = n """
    """ def traversal_handler(node, depth):
        print('%s+ %s %.3f' % (
            '  ' * depth,
            node.product.process.name,
            node.result
        ))

    impact = olca.ref(
        olca.ImpactCategory,
        'd9ff50c2-00ab-3901-8970-569f9b7c8041')
    print(impact)
    tree = client.upstream_tree_of(result, impact)

    tree.traverse(traversal_handler) """

    # the result remains accessible (for exports etc.) until
    # you dispose it, which you should always do when you do
    # not need it anymore
    client.dispose(result)

    return json

if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)